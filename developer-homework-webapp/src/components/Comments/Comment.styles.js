import Reply from "@material-ui/icons/Reply";
import { Button } from "../Buttons/Button";
import { Textarea } from "../Inputs/PostBox";
import { Paragraph } from "../Paragraphs/Paragraph";
import { PostContainer } from "../Post/Post.styles";
import styled from "styled-components";

export const Body = styled(Paragraph)`
  color: black;
  font-size: 16px;
`;
export const CommentContainer = styled(PostContainer)`
  background: ${props => props.theme.white};
  width: 100%;
`;
export const CommentReplyContainer = styled.div`
  display: flex;
  align-items: center;
`;
export const Arrow = styled(Reply)`
  fill: ${props => props.theme.light}!important;
`;
export const ArrowButton = styled(Reply)`
  fill: ${props => props.theme.dark}!important;
  font-size: 14px !important;
`;
export const ToggleComments = styled(Button)`
  color: grey;
  border: 1px solid grey;
  background: ${props => props.theme.white};
  font-size: 14px;
  width: 100px;
  height: 40px;
  margin-right: 20px;
  flex: 1;
`;
export const ReplyButton = styled(ToggleComments)`
  color: ${props => props.theme.dark};
  flex: 1;
  margin-right: 0;
`;
export const AddCommentContainer = styled(CommentReplyContainer)`
  margin-top: 20px;
`;
export const CommentTextarea = styled(Textarea)`
  font-size: 16px;
  margin-top: 0;
  height: 48px;
  width: 100%;
  max-width: 315px;
  color: ${props => props.theme.dark};
  background: ${props => props.theme.white};
  border: 2px solid ${props => props.theme.light};
  border-radius: 1px;
  &:focus {
    outline: ${props => props.theme.dark} auto 1px;
  }
  flex: 1;
`;
export const CommentButton = styled(ToggleComments)`
  color: ${props => props.theme.dark};
  margin-right: 0;
  margin-left: 20px;
`;
