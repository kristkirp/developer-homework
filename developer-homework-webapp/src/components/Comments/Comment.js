import React, { useState, useEffect } from "react";
import {
  Body,
  CommentContainer,
  CommentReplyContainer,
  Arrow,
  ArrowButton,
  ToggleComments,
  ReplyButton,
  AddCommentContainer,
  CommentTextarea,
  CommentButton
} from "./Comment.styles";
import { TextContainer } from "../Post/Post.styles";

const axios = require("axios");

const Comment = ({ postId }) => {
  const [comments, setComments] = useState("");
  const [comment, setComment] = useState("");
  const [shouldDisplayComments, setShouldDisplayComments] = useState(false);
  const [shouldDisplayReplyTextarea, setShouldDisplayReplyTextarea] = useState(
    false
  );
  const length = comments ? comments.length : 0;

  useEffect(() => {
    getComments();
  }, []);

  async function getComments() {
    try {
      const response = await axios.get(`/posts/${postId}/comments`);
      setComments(response.data);
    } catch (error) {
      console.error(error);
    }
  }
  async function addComment() {
    if (comment.length > 0) {
      try {
        const response = await axios.post(`posts/${postId}`, { text: comment });
        getComments();
      } catch (error) {
        console.error(error);
      }
    }
  }

  return (
    <>
      <TextContainer>
        <ToggleComments
          onClick={() => setShouldDisplayComments(!shouldDisplayComments)}
        >
          {length} Comments
        </ToggleComments>
        <ReplyButton
          onClick={() =>
            setShouldDisplayReplyTextarea(!shouldDisplayReplyTextarea)
          }
        >
          <ArrowButton /> Reply
        </ReplyButton>
      </TextContainer>
      {shouldDisplayReplyTextarea && (
        <AddCommentContainer>
          <CommentTextarea onChange={e => setComment(e.target.value)} />
          <CommentButton onClick={addComment}>Add comment</CommentButton>
        </AddCommentContainer>
      )}
      {comments &&
        shouldDisplayComments &&
        comments.map((comment, index) => {
          return (
            shouldDisplayComments && (
              <CommentReplyContainer key={index}>
                <Arrow />
                <CommentContainer key={index}>
                  <Body>{comment.body}</Body>
                </CommentContainer>
              </CommentReplyContainer>
            )
          );
        })}
    </>
  );
};

export default Comment;
