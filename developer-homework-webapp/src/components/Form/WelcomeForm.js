import React, { useState } from "react";
import { Link } from "react-router-dom";
import { Button } from "../Buttons/Button";
import TextInput from "../Inputs/TextInput";
import { Paragraph } from "../Paragraphs/Paragraph";
import styled, { ThemeProvider } from "styled-components";
import themeMain from "../../theme";
import Geosuggest from "react-geosuggest";

const FormWrapper = styled.div`
  margin-top: 1em;
  display: flex;
  align-items: center;
  height: 100%;
  justify-content: center;
`;
const Container = styled.div`
  width: 400px;
  margin-left: auto;
  margin-right: auto;
  position: absolute;
  top: 50%;
  left: 50%;
  -moz-transform: translateX(-50%) translateY(-50%);
  -webkit-transform: translateX(-50%) translateY(-50%);
  transform: translateX(-50%) translateY(-50%);
  &.option-chosen .geosuggest__suggests-wrapper {
    display: none;
  }
`;
const Warning = styled(Paragraph)`
  color: ${props => props.theme.secondary};
`;
const ButtonDone = styled(Button)`
  color: ${props => props.theme.secondary};
  border: 3px solid ${props => props.theme.secondary};
  background: ${props => props.theme.main};
`;
const Geo = styled(Geosuggest)`
  font-size: 22px;
  height: 48px;
  width: 100%;
  color: ${props => props.theme.secondary};
  background: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
  margin-bottom: 15px;
  &:focus {
    outline: ${props => props.theme.secondary} auto 5px;
  }
`;
const Nav = styled.div`
  margin-top: 200px;
`;
function WelcomeForm() {
  const [username, setUsername] = useState("");
  const [city, setCity] = useState("");
  const [lat, setLat] = useState("");
  const [lon, setLon] = useState("");
  const [hideClassname, setHideClassname] = useState("");

  // useEffect(() => {
  //   console.log(city);
  // }, [city]);

  function getLocation(object) {
    if (object !== null && object !== undefined) {
      console.log(object);
      setCity(object.label);
      setLat(object.location.lat);
      setLon(object.location.lng);
      setHideClassname("option-chosen");
    }
  }
  return (
    <ThemeProvider theme={themeMain}>
      <FormWrapper>
        <Warning>Please enter your username and city to get started</Warning>
        <Container className={hideClassname}>
          <TextInput onChange={e => setUsername(e.target.value)} />
          <Geo
            onSuggestSelect={e => getLocation(e)}
            country={"CA"}
            focus
            style={{
              input: {
                height: "48px",
                width: "100%",
                color: `${themeMain.secondary}`,
                background: `${themeMain.main}`,
                fontSize: "22px",
                border: "none",
                borderRadius: "3px"
              },
              suggestItem: {
                fontSize: "12px"
              }
            }}
          />
          <Nav>
            {username.length > 0 && city.length > 0 ? (
              <Link
                to={{
                  pathname: "/posts",
                  state: { username: username, city: city, lat: lat, lon: lon }
                }}
              >
                <ButtonDone>Done</ButtonDone>
              </Link>
            ) : (
              <ButtonDone>Done</ButtonDone>
            )}
          </Nav>
        </Container>
      </FormWrapper>
    </ThemeProvider>
  );
}

export default WelcomeForm;
