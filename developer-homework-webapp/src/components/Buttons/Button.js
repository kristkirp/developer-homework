import styled from "styled-components";

export const Button = styled.button`
  font-size: 22px;
  width: 100%;
  height: 48px;
  border-radius: 3px;
`;
