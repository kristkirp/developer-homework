import React from "react";
import styled from "styled-components";
import colors from "../../theme";

const Input = styled.input`
  font-size: 22px;
  height: 48px;
  width: 100%;
  color: ${props => props.theme.secondary};
  background: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
margin-bottom: 15px;
  &:focus {
    outline: ${props => props.theme.secondary} auto 5px;
  }
`;

const TextInput = ({ onChange }) => {
  let inputRef = React.createRef();
  return (
    <Input
      theme={colors}
      ref={inputRef}
      onChange={onChange}
      placeholder="Username"
      onMouseEnter={() => {
        inputRef.current.focus();
      }}
    />
  );
}

export default TextInput;
