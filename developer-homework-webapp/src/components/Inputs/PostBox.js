import React from "react";
import styled from "styled-components";
import themeMain from "../../theme";

export const Textarea = styled.textarea`
  font-size: 22px;
  height: 88px;
  width: 100%;
  color: ${props => props.theme.secondary};
  background: ${props => props.theme.main};
  border: none;
  border-radius: 3px;
  &:focus {
    outline: ${props => props.theme.secondary} auto 5px;
  }
`;

const PostBox = ({ onChange, placeholder }) => {
  let inputRef = React.createRef();
  return (
    <Textarea
      theme={themeMain}
      ref={inputRef}
      onChange={onChange}
      placeholder={placeholder}
      onMouseEnter={() => {
        inputRef.current.focus();
      }}
    />
  );
};

export default PostBox;
