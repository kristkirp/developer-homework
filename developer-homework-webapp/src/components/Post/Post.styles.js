import Reply from "@material-ui/icons/Reply";
import Account from "@material-ui/icons/AccountBox";
import { Button } from "../Buttons/Button";
import { Textarea } from "../Inputs/PostBox";
import { Paragraph } from "../Paragraphs/Paragraph";
import styled from "styled-components";

export const Container = styled.div`
  display: flex;
  background: ${props => props.theme.mainBackground};
  padding: 20px;
  @media (max-width: 491px){
    display: block;
    width: 100%;
    padding: 20px 0 0 0;
    height: 100%;
  }
`;

export const ProfileContainer = styled.div`
  height: 462px;
  border: 2px solid ${props => props.theme.light};
  background: ${props => props.theme.main};
  flex: 1;
  text-align: center;
  @media (max-width: 491px){
    max-width: 300px;
    margin: 0 auto;
    flex: unset;
    height: auto;

  }
`;

export const ProfileImage = styled(Account)`
  font-size: 170px !important;
  fill: ${props => props.theme.light}!important;
`;

export const ProfileGreetContainer = styled.div`
  flex-wrap: wrap;
  overflow-wrap: break-word;
`;

export const ProfileGreet = styled(Paragraph)`
  color: ${props => props.theme.dark};
`;

export const AddContainer = styled.div`
  width: auto;
  margin: 0 50px;
  flex: 2;
  @media (max-width: 491px){
    flex: unset;
    margin-top: 20px;
  }
`;
export const PostsContainer = styled.div``;

export const PostContainer = styled.div`
  background: ${props => props.theme.main};
  border: 2px solid ${props => props.theme.light};
  margin: 20px 0;
  padding-left: 10px;
`;

export const TextContainer = styled.div`
  display: flex;
  width: 100%;
`;

export const Title = styled(Paragraph)`
  color: ${props => props.theme.dark};
`;

export const Body = styled(Paragraph)`
  color: black;
  font-size: 16px;
`;

export const Date = styled(Paragraph)`
  color: ${props => props.theme.dark};
  font-size: 14px;
`;

export const City = styled(Date)`
  margin-right: 15px;
`;

export const EmptyContainer = styled.div`
  width: auto;
  margin: 0 50px;
  flex: 2;
  @media (max-width: 491px){
    flex: unset;
  }
`;

export const ParagraphEmpty = styled(Paragraph)`
  margin-bottom: 50px;
  color: ${props => props.theme.tertiary};
`;

export const ButtonAdd = styled(Button)`
  color: ${props => props.theme.secondary};
  border: 3px solid ${props => props.theme.secondary};
  background: ${props => props.theme.main};
  margin-top: 30px;
`;

export const CommentContainer = styled(PostContainer)`
  background: ${props => props.theme.white};
`;

export const CommentReplyContainer = styled.div`
  display: flex;
  align-items: center;
`;

export const Arrow = styled(Reply)`
  fill: ${props => props.theme.light}!important;
`;

export const ArrowButton = styled(Reply)`
  fill: ${props => props.theme.dark}!important;
  font-size: 14px !important;
`;

export const ToggleComments = styled(Button)`
  color: grey;
  border: 1px solid grey;
  background: ${props => props.theme.white};
  font-size: 14px;
  width: 100px;
  height: 40px;
  margin-right: 20px;
`;

export const ReplyButton = styled(ToggleComments)`
  color: ${props => props.theme.dark};
`;

export const AddCommentContainer = styled(CommentReplyContainer)`
  margin-top: 20px;
`;

export const CommentTextarea = styled(Textarea)`
  font-size: 16px;
  margin-top: 0;
  height: 48px;
  width: 100%;
  max-width: 315px;
  color: ${props => props.theme.dark};
  background: ${props => props.theme.white};
  border: 2px solid ${props => props.theme.light};
  border-radius: 1px;
  &:focus {
    outline: ${props => props.theme.dark} auto 1px;
  }
`;

export const CommentButton = styled(ToggleComments)`
  color: ${props => props.theme.dark};
  margin-right: 0;
  margin-left: 20px;
`;
