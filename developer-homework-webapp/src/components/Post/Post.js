import React, { useState, useEffect } from "react";
import moment from "moment";
import PostBox from "../Inputs/PostBox";
import Comment from "../Comments/Comment";
import { ThemeProvider } from "styled-components";
import themeMain from "../../theme";
import {
  Container,
  ProfileContainer,
  ProfileGreetContainer,
  ProfileImage,
  ProfileGreet,
  AddContainer,
  PostsContainer,
  PostContainer,
  TextContainer,
  Title,
  Body,
  Date,
  City,
  EmptyContainer,
  ParagraphEmpty,
  ButtonAdd
} from "./Post.styles";

const axios = require("axios");
const kelvinToCelsius = require('kelvin-to-celsius');

function Post(props) {
  const { username } = props.location.state;
  const { city } = props.location.state;
  const { lat } = props.location.state;
  const { lon } = props.location.state;
  let placeName = city.split(",")[0];
  const Api_Key = "cc11e6b703119b14f6279f70e08b6e20";

  const [user, setUser] = useState("");
  const [post, setPost] = useState("");
  const [data, setData] = useState(null);
  const [weather, setWeather] = useState({});
  const emptyPost = "You currently don't have any posts";
  const startWriting = "Start writing your post";

  useEffect(() => {
    async function getWeather() {
      const apiCall = await axios.get(
        `http://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${Api_Key}`
      );
      console.log(apiCall.data)
      setWeather(apiCall);
    }
    async function createUser() {
      try {
        const response = await axios.post(`/users/new`, { userName: username });
        setUser(response.data);
      } catch (error) {
        console.error(error);
      }
    }
    createUser();
    getWeather();
  }, []);

  async function addPost() {
    try {
      if (post !== "" && post !== undefined) {
        await axios.post("/posts", { text: post, user: user });
        getAllPosts();
      }
    } catch (error) {
      console.error(error);
    }
  }
  async function getAllPosts() {
    try {
      const response = await axios.get("/posts");
      setData(response.data);
    } catch (error) {
      console.error(error);
    }
  }

  return (
    <ThemeProvider theme={themeMain}>
      <Container>
        <ProfileContainer>
          <ProfileImage />
          <ProfileGreetContainer>
            <ProfileGreet>welcome {username}</ProfileGreet>
          </ProfileGreetContainer>
        </ProfileContainer>
        {!data ? (
          <EmptyContainer>
            <ParagraphEmpty>{emptyPost}</ParagraphEmpty>
            <PostBox
              onChange={e => setPost(e.target.value)}
              placeholder={startWriting}
            />
            <ButtonAdd onClick={addPost}>Add post</ButtonAdd>
          </EmptyContainer>
        ) : (
          <AddContainer>
            <PostBox
              onChange={e => setPost(e.target.value)}
              placeholder={startWriting}
            />
            <ButtonAdd onClick={addPost}>Add post</ButtonAdd>

            <PostsContainer>
              {data.map((post, index) => {
                let dateTime = new window.Date(post.creationDate);
                dateTime = moment(dateTime).format("DD MMM YYYY");
                return (
                  <>
                    <PostContainer key={index}>
                      <Title>{post.title}</Title>
                      <Body>{post.body}</Body>
                      <TextContainer>
                        <TextContainer>
                          <City>{placeName}, {Math.floor(kelvinToCelsius(weather.data.main.temp))} °C</City>
                        </TextContainer>
                        <TextContainer>
                          <Date>{dateTime}</Date>
                        </TextContainer>
                        <TextContainer>
                          <City>
                            lat:{lat} lon:{lon}
                          </City>
                        </TextContainer>
                      </TextContainer>
                    </PostContainer>
                    <Comment postId={post.id} />
                  </>
                );
              })}
            </PostsContainer>
          </AddContainer>
        )}
      </Container>
    </ThemeProvider>
  );
}

export default Post;
