import React from "react";
import { BrowserRouter as Router, Switch, Route } from "react-router-dom";

import WelcomeForm from "./components/Form/WelcomeForm";
import Post from "./components/Post/Post";

function App() {
  return (
    <Router>
      <Switch>
        <Route exact path="/" component={WelcomeForm} />
        <Route exact path="/posts" component={Post} />
      </Switch>
    </Router>
  );
}

export default App;
