const themeMain = {
    main: "papayawhip",
    secondary: "palevioletred",
    tertiary: "mediumpurple",
    dark: "indigo",
    light: "gainsboro",
    mainBackground: "mintcream",
    white: "#fbfbfb",
};

export default {
    ...themeMain
}