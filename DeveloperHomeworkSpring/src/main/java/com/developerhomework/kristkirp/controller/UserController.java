package com.developerhomework.kristkirp.controller;

import com.developerhomework.kristkirp.model.User;
import com.developerhomework.kristkirp.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("users")
public class UserController {

    @Autowired
    private UserService userService;

    @PostMapping("/new")
    public User postMessage(@RequestBody User user) {
        return userService.updateUser(user);
    }
}
