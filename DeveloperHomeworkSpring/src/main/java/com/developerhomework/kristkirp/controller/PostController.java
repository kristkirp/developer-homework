package com.developerhomework.kristkirp.controller;

import com.developerhomework.kristkirp.dto.BodyDto;
import com.developerhomework.kristkirp.dto.PostDto;
import com.developerhomework.kristkirp.model.Comment;
import com.developerhomework.kristkirp.model.Post;
import com.developerhomework.kristkirp.service.PostService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;

@RestController
@RequestMapping("/posts")
public class PostController {

    @Autowired
    private PostService postService;

    @PostMapping
    public Post createPost(@Valid @RequestBody PostDto dto) {
        return postService.updatePost(new Post(dto.getText(), dto.getUser()));
    }

    @PostMapping("/{postId}")
    public Comment saveComment(@Valid @RequestBody BodyDto dto, @PathVariable String postId) {
        Post post = postService.findPostById(Integer.parseInt(postId));
        Comment comment = new Comment(dto.getText(), post);
        return postService.updateComment(comment);
    }

    @GetMapping
    public List<Post> getPosts() {
        return postService.getAllPosts();
    }

    @GetMapping("{postId}/comments")
    public List<Comment> getCommentsByPostId(@PathVariable String postId) {
        return postService.findCommentsByPost(postId);
    }
}
