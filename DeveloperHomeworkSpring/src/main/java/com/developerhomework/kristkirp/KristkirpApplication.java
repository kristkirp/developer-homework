package com.developerhomework.kristkirp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class KristkirpApplication {

    public static void main(String[] args) {
        SpringApplication.run(KristkirpApplication.class, args);
    }

}
