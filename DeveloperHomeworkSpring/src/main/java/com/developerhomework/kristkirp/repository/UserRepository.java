package com.developerhomework.kristkirp.repository;

import com.developerhomework.kristkirp.model.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends CrudRepository<User, Integer> {
}
