package com.developerhomework.kristkirp.repository;

import com.developerhomework.kristkirp.model.Comment;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface CommentRepository extends CrudRepository<Comment, Integer> {
    @Query(value = "SELECT * FROM Comment comm WHERE comm.post_id = ?1", nativeQuery = true)
    List<Comment> findCommentsByPost(int postId);
}
