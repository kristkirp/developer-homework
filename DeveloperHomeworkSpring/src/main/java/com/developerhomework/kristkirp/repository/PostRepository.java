package com.developerhomework.kristkirp.repository;

import com.developerhomework.kristkirp.model.Post;
import org.springframework.data.repository.CrudRepository;

public interface PostRepository extends CrudRepository<Post, Integer> {
}
