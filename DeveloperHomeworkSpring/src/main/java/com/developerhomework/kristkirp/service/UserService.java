package com.developerhomework.kristkirp.service;

import com.developerhomework.kristkirp.model.User;
import com.developerhomework.kristkirp.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepository;

    public User updateUser(User user) {
        return userRepository.save(user);
    }

    public User getUserById(int userId) {
        return userRepository.findById(userId).orElse(new User());
    }

}
