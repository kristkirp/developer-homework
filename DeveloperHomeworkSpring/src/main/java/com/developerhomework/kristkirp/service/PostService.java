package com.developerhomework.kristkirp.service;

import com.developerhomework.kristkirp.model.Comment;
import com.developerhomework.kristkirp.model.Post;
import com.developerhomework.kristkirp.model.User;
import com.developerhomework.kristkirp.repository.CommentRepository;
import com.developerhomework.kristkirp.repository.PostRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PostService {

    @Autowired
    private PostRepository postRepository;

    @Autowired
    private CommentRepository commentRepository;

    public List<Post> getAllPosts() {
        List<Post> posts = new ArrayList<>();
        postRepository.findAll().forEach(posts::add);
        return posts;
    }

    public Post updatePost(Post post) {
        return postRepository.save(post);
    }

    public Post findPostById(int postId) {
        User user = new User();
        return postRepository.findById(postId).orElse(new Post("sample", user));
    }

    public Comment updateComment(Comment comment) {
       return commentRepository.save(comment);
    }

    public List<Comment> findCommentsByPost(String postId) {
        return commentRepository.findCommentsByPost(Integer.parseInt(postId));
    }
}
