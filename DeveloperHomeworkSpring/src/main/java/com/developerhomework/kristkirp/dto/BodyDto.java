package com.developerhomework.kristkirp.dto;

import com.developerhomework.kristkirp.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@NoArgsConstructor
@Data
public class BodyDto {
    private String text;

    public BodyDto(String text) {
        this.text = text;
    }
}
