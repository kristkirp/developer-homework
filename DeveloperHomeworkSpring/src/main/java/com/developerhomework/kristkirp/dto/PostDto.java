package com.developerhomework.kristkirp.dto;

import com.developerhomework.kristkirp.model.User;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
public class PostDto {
    private String text;
    private User user;

    public PostDto(String text, User user) {
        this.text = text;
        this.user = user;
    }
}
